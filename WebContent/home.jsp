<html>
 <head><title>Home Page</title></head>
 <body>
 <% 
 if(session.getAttribute("loggedin") == null || session.getAttribute("loggedin").equals("false"))
 {
	response.sendRedirect("notloggedin.jsp"); 
 } 
 %> 
 
  <h2>Home Page</h2>
	<ul>
		<li>
			<a href="/CSE135/login.jsp"> Login page </a>
		</li>
		<li>
			<a href="/CSE135/signup.html"> Sign up page </a>
		</li>
		<li>
			<a href="/CSE135/categories.jsp"> Categories </a>
		</li>
	</ul>
 </body>
</html>